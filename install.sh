#!/bin/bash
# assusmes using bash 4
# Created by Marcin Natanek

# essential programs:
# tree git bash

home=~
working_tree=`pwd`
timestamp=`date +"%d_%m_%Y-%H_%M_%S"`
backup_dir=$home/.dotfiles.bak/$timestamp
cp_command_file="cp -v"
cp_command_dir="rsync -avh"
dry_run=0
install=0
pull=0
refresh=0

declare -A files
files=(
["vimrc"]=.vimrc
["tmux.conf"]=.tmux.conf
["gitconfig"]=.gitconfig
["fishfun/"]=.config/fish/functions/
["fishrc"]=.config/fish/config.fish
["bin/"]=.bin/
)

die_usage(){
	echo ""
	echo " USAGE: $0 [ -d ] [ -i | -p | -r ]"
	echo "			-d	will perform a dry run, printing what will be done"
	echo "				and	executing no commands. To be used with other options."
	echo "			-i	will backup configs and install dotfiles from repo."
	echo "			-p	will copy current configs from system to the repo folder."
	echo "			-r	will refresh installed configs without running postscripts."
	echo ""
	exit 1
}

install_dots(){
	echo ""
	[ $install -eq 1 ] && echo "Files to be installed:"
	[ $refresh -eq 1 ] && echo "Files to be refreshed:"
	for file in ${!files[@]}
	do
		if [ -d $file ]; then
			echo " - [dir] $file"
		else
			echo " - $file"
		fi
	done
	echo ""

	if [ ! -e $backup_dir ]; then
		echo "Creating backup directory in $backup_dir"
		echo ""
		[ $dry_run -eq 0 ] && mkdir -p $backup_dir
	fi

	[ $install -eq 1 ] && echo "Installing..."
	[ $refresh -eq 1 ] && echo "Refreshing..."
	for file in  ${!files[@]}
	do
		name=${files["$file"]}
		echo " > Processing $file"
		if [ -e $home/$name ]; then
			if [ -d $file ]; then
				echo "   | Backing up dir $home/$name to $backup_dir/$name"
				command="$cp_command_dir $home/$name $backup_dir/$name"
			else
				echo "   | Backing up file $home/$name to $backup_dir/$name"
				command="$cp_command_file $home/$name $backup_dir/$name"
			fi
			echo "   |   $command"
			[ $dry_run -eq 0 ] && $command |& sed "s/^/   |   > /"
		fi
		if [ -d $file ]; then
			echo "   | Applying new dir"
			command="$cp_command_dir $file $home/$name"
		else
			echo "   | Applying new file"
			command="$cp_command_file $file $home/$name"
		fi
		echo "   |   $command"
		[ $dry_run -eq 0 ] && $command |& sed "s/^/   |   > /"
	done

	if [ $install -eq 1 ]; then
		postscripts=`ls postscripts`
		echo ""
		echo "Found postcripts:"
		for script in ${postscripts[@]}
		do
			echo " - $script"
		done

		echo ""
		echo "Running postscripts..."
		for script in ${postscripts[@]}
		do
			echo " > Running $script"
			chmod +x postscripts/$script
			[ $dry_run -eq 0 ] && postscripts/$script | tee $backup_dir/$script-log |& sed "s/^/   |   > /"
		done
	fi 

	echo ""
	echo "Backup dir:"
	tree -a $backup_dir;
	echo ""
	echo "Done."
}

pull_dots(){
	echo ""
	echo "Pulling config files from system to the working tree:"
	for file in ${!files[@]}
	do
		echo " - $file"
	done
	echo ""

	echo "Copying..."
	for file in ${!files[@]}
	do
		name=${files["$file"]}
		echo " > Pulling $file"
		if [ -e $home/$name ]; then
			if [ -d $file ]; then
				echo "   | Directory exists: $home/$name"
				command="$cp_command_dir $home/$name $file"
			else
				echo "   | File exists: $home/$name"
				command="$cp_command_file $home/$name $file"
			fi
			echo "   |   $command"
			[ $dry_run -eq 0 ] && $command |& sed "s/^/   |   > /"
		fi
	done

	echo ""
	echo "git status:"
	git status
	echo ""
	echo "Done."

}

while getopts ":ipdr" opt
do
	case $opt in
		i)
			[ $pull -eq 1 ] && die_usage
			[ $refresh -eq 1 ] && die_usage
			install=1
			;;
		p)	
			[ $install -eq 1 ] && die_usage
			[ $refresh -eq 1 ] && die_usage
			pull=1
			;;
		r)	
			[ $install -eq 1 ] && die_usage
			[ $pull -eq 1 ] && die_usage
			refresh=1
			;;
		d)	
			echo "Dry run mode enabled (no commands will be executed)"
			dry_run=1
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			die_usage
			;;
	esac
done

# MAIN


[ $install -eq 0 ] && [ $refresh -eq 0 ] && [ $pull -eq 0 ] && die_usage

[ $install -eq 1 ] && install_dots
[ $refresh -eq 1 ] && install_dots
[ $pull -eq 1 ] && pull_dots
