# mnatan's dotconfig repo #

### What is this repository for? ###

This repository contains all the config files I use with my favorite programs.
For now it setups:

* vim
    * vundle
    * airline
    * fugitive
    * nerdtree
    * CtrlP
    * commentary
* tmux
* fish
* powerline fonts

### Installation ###
1. `git clone git@bitbucket.org:mnatan/dotfiles.git`
2. `cd dotfiles`
3. Run install script. -d flag prevents copying and informs you what would be done:
`./install.sh -di`
4. To complete installation run:
`./install.sh -i`