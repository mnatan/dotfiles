#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;
use Net::DBus;

my $symbol = "♫";

my $bus     = Net::DBus->session;
my $spotify = $bus->get_service("org.mpris.MediaPlayer2.spotify");
my $manager = $spotify->get_object( "/org/mpris/MediaPlayer2",
    "org.freedesktop.DBus.Properties" );
my %metadata
    = %{ $manager->Get( 'org.mpris.MediaPlayer2.Player', 'Metadata' ) };

my $artist = join ", ", @{ $metadata{'xesam:artist'} };
my $title = $metadata{'xesam:title'};
print "$symbol $artist - $title$symbol";
